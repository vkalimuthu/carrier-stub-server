Setup Instructions
-

1. Clone the repository

	`git clone git@bitbucket.org:vkalimuthu/carrier-stub-server.git`

2. Install the dependencies

	`npm install`

3. Start the stub server

	`sudo npm start`

4. Enjoy the stub server on http://127.0.0.1 

