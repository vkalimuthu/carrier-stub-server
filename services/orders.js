var orders = {
    path: '/v3/orders',
    template: require('../stubs/orders.json')
};

module.exports = [orders];