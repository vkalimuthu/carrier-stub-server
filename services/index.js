var endpoints = [
    {
        method: "GET",
        url: '/v3/orders'
    },
    {
        method: "GET",
        url: '/v3/carrier/carrier-order-submissions'
    }
];

module.exports = {
	path: '/',
	render: function(req, res) {

		var html = '<h1>Carrier Stub Server</h1>';

        html += "<h3> Available endpoints </h3><ul>";
        
        html += endpoints.map(function(endpoint) {
			return '<li>' + endpoint.method + ' <a href="' + endpoint.url + '">' + endpoint.url + '</a></li>';
        }).join('')
        
        html += "</ul>";

		res.send(200, html);
	}
};